<!--
 * Yo Voy (index.php) code is distributed under LGPL 3
 * Copyright (C) 2015 Camilo Villavicencio <contacto at cvillavicencio dot com>
 *
 * This code is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 -->
<title>YoVoy 0.1a</title>		
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<body>	
<center><b>YoVoy</b> v0.1a<br>

<!-- script que advierte que faltan datos. Lo saqué de snaphost.com -->
<script type="text/javascript">
function ValidateForm(frm) {
if (frm.First_Name.value == "") {alert('Falta tu nombre.');frm.First_Name.focus();return false;}
if (frm.Last_Name.value == "") {alert('Falta tu apellido.');frm.Last_Name.focus();return false;}
if (frm.Email_Address.value == "") {alert('Falta tu correo.');frm.Email_Address.focus();return false;}
if (frm.CaptchaCode.value == "") {alert('Confirma que no eres un robot.');frm.CaptchaCode.focus();return false;}
return true; }
function ReloadCaptchaImage(captchaImageId) {
var obj = document.getElementById(captchaImageId);
var src = obj.src;
var date = new Date();
var pos = src.indexOf('&rad=');
if (pos >= 0) { src = src.substr(0, pos); }
obj.src = src + '&rad=' + date.getTime();
return false; }
</script>
<hr />
<form id="ExampleForm" action="resultado.php"  method="post">
<input id="SnapHostID" name="SnapHostID" type="hidden" value="Q3X8LWMNEFCZ" />
<table border="0" cellpadding="5" cellspacing="0">
<tr>
<td style="width:50%">
<b>Nombre</b><br />
<input name="nombre" type="text" maxlength="50" style="width:260px" />
</td>
<td style="width:50%">
<b>Apellido</b><br />
<input name="apellido" type="text" maxlength="50" style="width:260px" />
</td>
</tr>
<tr>
<td colspan="2">
<b>Email *</b><br />
<input name="mail" type="text" maxlength="100" style="width:535px" />
</td>
</tr>
</table>
<br />
<div style="text-align:center;">
<table border="0" cellpadding="0" cellspacing="0" style="text-align:center; width:300px; margin:auto;">
<tr>
<td><i>No eres un robot?</i></td>
<td></td>
</tr>
<tr>
<td>
<input name="CaptchaCode" type="text" maxlength="6"
style="width:130px; height:28px; font-size:24px; text-align:center;" />
</td>
<td>
<img id="CaptchaImage"
style="border:1px solid #999999; vertical-align:bottom;" alt="captcha" title="captcha"
src="http://www.SnapHost.com/captcha/WebForm.aspx?id=Q3X8LWMNEFCZ&ImgType=2" />
<br /><a href="#" onclick="return ReloadCaptchaImage('CaptchaImage');"><span style="font-size:12px;">recargar imagen</span></a>
</td>
</tr>
</table>
<br /><br />
<input name="Submit" type="submit" value="¡Yo voy!" />
</div>
</form>
